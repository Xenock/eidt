require 'test_helper'

class EstaticosControllerTest < ActionController::TestCase
  test "should get inicio" do
    get :inicio
    assert_not_nil assigns :frases
    assert_response :success
  end

  test "should get escuela" do
    get :escuela
    assert assigns :planes
    assert_not_nil assigns :instructores
    assert_response :success
  end

  test "should get visitas" do
    get :visitas
    assert_not_nil assigns :visitas
    assert_response :success
  end

  test "should get mercado" do
    get :mercado
    assert_not_nil assigns :animales
    assert_response :success
  end

  test "should get contacto" do
    get :contacto
    assert_response :success
  end

end
