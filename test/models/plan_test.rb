require 'test_helper'

class PlanTest < ActiveSupport::TestCase

  def setup
    @plan = plans(:one)
  end

  test "No debe guardar plan vacio" do
    plan = Plan.new
    assert_not plan.save, "Guardado con propiedades vacias"
  end

  test "Nombre debe estar presente" do
    plan = @plan
    plan.nombre = nil
    assert_not plan.valid?
  end

  test "Descripcion debe estar presente" do
    plan = @plan
    plan.descripcion = nil
    assert_not plan.valid?
  end

  test "Precio debe estar presente" do
    plan = @plan
    plan.precio = nil
    assert_not plan.valid?
  end

  test "Nombre debe ser igual o mayor a 3 caracteres" do
    plan = @plan
    plan.nombre = "a" * 2
    assert_not plan.valid?
    plan.nombre = "a" * 3
    assert plan.valid?
    plan.nombre = "a" * 4
    assert plan.valid?
  end

  test "Descripcion debe ser igual o mayor a 3 caracteres" do
    plan = @plan
    plan.descripcion = "a" * 2
    assert_not plan.valid?
    plan.descripcion = "a" * 3
    assert plan.valid?
    plan.descripcion = "a" * 4
    assert plan.valid?
  end

  test "Precio debe ser un numero" do
    plan = @plan
    plan.precio = "a"
    assert_not plan.valid?
    plan.precio = 1
    assert plan.valid?
  end
end
