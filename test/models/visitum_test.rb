require 'test_helper'

class VisitumTest < ActiveSupport::TestCase

    def setup
      @visita = visita(:one)
    end

    test "No debe guardar visita vacio" do
      visita = Visitum.new
      assert_not visita.save, "Guardado con propiedades vacias"
    end

    test "Nombre debe estar presente" do
      visita = @visita
      visita.nombre = nil
      assert_not visita.valid?
    end

    test "Descripcion debe estar presente" do
      visita = @visita
      visita.descripcion = nil
      assert_not visita.valid?
    end

    test "Precio debe estar presente" do
      visita = @visita
      visita.precio = nil
      assert_not visita.valid?
    end

    test "Nombre debe ser igual o mayor a 3 caracteres" do
      visita = @visita
      visita.nombre = "a" * 2
      assert_not visita.valid?
      visita.nombre = "a" * 3
      assert visita.valid?
      visita.nombre = "a" * 4
      assert visita.valid?
    end

    test "Descripcion debe ser igual o mayor a 3 caracteres" do
      visita = @visita
      visita.descripcion = "a" * 2
      assert_not visita.valid?
      visita.descripcion = "a" * 3
      assert visita.valid?
      visita.descripcion = "a" * 4
      assert visita.valid?
    end

    test "Precio debe ser un numero" do
      visita = @visita
      visita.precio = "a"
      assert_not visita.valid?
      visita.precio = 1
      assert visita.valid?
    end
end
