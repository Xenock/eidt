require 'test_helper'

class FraseTest < ActiveSupport::TestCase

  test "Texto debe estar presente" do
    frase = Frase.new
    assert_not frase.valid?
  end

  test "Texto debe ser menor a 30 caracteres" do
    frase = Frase.new
    frase.texto = "a" * 51
    assert_not frase.valid?
    frase.texto = "a" * 50
    assert frase.valid?
  end
end
