require 'test_helper'

class InstructorTest < ActiveSupport::TestCase

  test "Nombre de instructor presente" do
    instructor = Instructor.new
    assert_not instructor.valid?
  end

  test "Nombre de instructor mayor a 3 caracteres" do
    instructor = Instructor.new
    instructor.nombre = "a" * 2
    assert_not instructor.valid?
    instructor.nombre = "a" * 3
    assert instructor.valid?
  end
end
