require 'test_helper'

class AnimalTest < ActiveSupport::TestCase

  def setup
    @animal = animals(:one)
  end

  test "No debe guardar animal vacio" do
    animal = Animal.new
    assert_not animal.save, "Guardado animal con propiedades vacias"
  end

  test "Nombre presente e igual o mayor de 3 digitos" do
    animal = @animal
    animal.nombre = "aa"
    assert_not animal.valid?, "Nombre de animal debe ser igual o mayor a 3 digitos"
    animal.nombre = "aaa"
    assert animal.valid?
  end

  test "Nombre presente" do
    animal = @animal
    animal.nombre = nil
    assert_not animal.valid?, "El nombre del animal debe estar presente"
  end

  test "Fecha de nacimiento presente" do
    animal = @animal
    animal.fecha_nacimiento = nil
    assert_not animal.valid?, "La fecha de nacimiento del animal debe estar presente"
  end
end
