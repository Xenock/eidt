Rails.application.routes.draw do

  devise_for :users
  scope '/admin' do
    resources :frases, :plans, :animals, :visita, :instructors
  end
  get 'escuela', to: 'estaticos#escuela'
  get 'visitas', to: 'estaticos#visitas'
  get 'mercado', to: 'estaticos#mercado'
  get 'contacto', to: 'estaticos#contacto'

  root 'estaticos#inicio'

end