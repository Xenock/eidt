class CreateAnimals < ActiveRecord::Migration
  def change
    create_table :animals do |t|
      t.string :nombre
      t.text :descripcion
      t.date :fecha_nacimiento

      t.timestamps null: false
    end
  end
end
