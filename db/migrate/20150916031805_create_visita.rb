class CreateVisita < ActiveRecord::Migration
  def change
    create_table :visita do |t|
      t.string :nombre
      t.text :descripcion
      t.integer :precio

      t.timestamps null: false
    end
  end
end
