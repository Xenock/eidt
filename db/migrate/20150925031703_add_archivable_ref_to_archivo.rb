class AddArchivableRefToArchivo < ActiveRecord::Migration
  def change
    add_reference :archivos, :archivable, polymorphic: true, index: true
  end
end
