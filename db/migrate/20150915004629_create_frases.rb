class CreateFrases < ActiveRecord::Migration
  def change
    create_table :frases do |t|
      t.text :texto

      t.timestamps null: false
    end
  end
end
