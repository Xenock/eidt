class CreateInstructors < ActiveRecord::Migration
  def change
    create_table :instructors do |t|
      t.string :nombre

      t.timestamps null: false
    end
  end
end
