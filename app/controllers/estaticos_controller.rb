class EstaticosController < ApplicationController
  def inicio
    @frases = Frase.all
  end

  def escuela
    @planes = Plan.all
    @instructores = Instructor.all
  end

  def visitas
    @visitas = Visitum.all
  end

  def mercado
    @animales = Animal.paginate(page: params[:page], per_page: 30)
  end

  def contacto
  end
end
