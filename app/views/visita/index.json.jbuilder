json.array!(@visita) do |visitum|
  json.extract! visitum, :id, :nombre, :descripcion, :precio
  json.url visitum_url(visitum, format: :json)
end
