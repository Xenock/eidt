json.array!(@frases) do |frase|
  json.extract! frase, :id, :texto
  json.url frase_url(frase, format: :json)
end
