json.array!(@animals) do |animal|
  json.extract! animal, :id, :nombre, :descripcion, :fecha_nacimiento
  json.url animal_url(animal, format: :json)
end
