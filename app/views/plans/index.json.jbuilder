json.array!(@plans) do |plan|
  json.extract! plan, :id, :nombre, :descripcion, :precio
  json.url plan_url(plan, format: :json)
end
