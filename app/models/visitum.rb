class Visitum < ActiveRecord::Base
  validates :nombre, presence: true, length: { minimum: 3}
  validates :descripcion, presence: true, length: { minimum: 3}
  validates :precio, presence: true, numericality: true
end
