class Animal < ActiveRecord::Base
  has_many :archivos, as: :archivable, dependent: :destroy
  accepts_nested_attributes_for :archivos
  validates :nombre, presence: true, length:{ minimum: 3 }
  validates :fecha_nacimiento, presence: true
end
