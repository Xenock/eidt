class Frase < ActiveRecord::Base
  validates :texto, presence: true, length: { maximum: 50}
end
