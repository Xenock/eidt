class Archivo < ActiveRecord::Base
  belongs_to :archivable, polymorphic: true
  mount_uploader :archivo, ArchivoUploader
end
