class Instructor < ActiveRecord::Base
  validates :nombre, presence: true, length: { minimum: 3}
end
